#!/bin/bash
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
#                           ..-.:.:...
#                        :.-- -     ..:...
#                    :.:. -             -.:...
#                :.:. -                     ..:...
#            :.:. .            _;:__.          ...-...
#        :.:. .               :;    -+_    -|       .--...
#    :.-- .                    -=      -~-.-           -.:...
#  -:...              ___.      -=_                        -.--
#  ...    .          =;  --=_     :=                   ..   ...
#  .-.      . .              ~-___=;               . -      .:.
#  ...           -.                             -.          ...
#  .:.                .                    . .              .:.
#  ...                   -.             -.                  ...
#  .:.                      ...    . -                -~4>  .:.
#  ...       _^+_.              -.                       2  ...
#  .:.           ~,              .                 /'   _(  .:.
#  ....           <              -          +'  ^LJ>   _^   ...
#  .:..          _);             -     _   J   _/  ~~-'    .:.
#  ....        _&i^i             .   _~_, <(   .^           ...
#   :.       _v>^  <             .  _X~'  -s,               .:.
#   :.             -=            .   S      ^'              ...
#   :....           -=_  ,       .   2                    ..-.:
#      :....          -^^        .                    .-.:. .
#         ..:...                 .                 ..:. -
#             -.:...             .           . :.-- -
#                  :....         .         -.-- .
#                     -.:...     .    ..:.: -
#                          -.:......--. -
#                              -.:. .
# 
# 
# Copyright (c) WIDE IO LTD
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of Wide.IO nor the names of others contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE WIDE.IO AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
## WIDE IO  - ALGOBOX
## ALL RIGHTS RESERVED
VERSION="0.2.0"


function ok_to_proceed() {
  answer=""
  ready=""
  while [  "$ready" = "y" ] ; do
    read -p "are you ok to proceed" answer 2>&1
    case $answer in
      y|Y|yes|Yes|YES) 
        answer=y
        ready=y
        ;;
      n|N|no|No|NO) 
        answer=n        
        ready=y
        ;;
      *)
        echo "sorry I haven't understood your reply..." 1>&2 
    esac
  done
}

function install_user_and_modify_bashrc() {
  ./setup.sh prepare
  echo "$PWD" > ~/.wideio-brige.path || return -1
  if ! grep "### WIDEIO-BRIDGE SECTION BEG" ~/.bashrc &> /dev/null; then
    cat << EOF >> ~/.bashrc
### WIDEIO-BRIDGE SECTION BEG
WIO_BRIDGE_PATH="$PWD"
export PATH=\$PATH:\$WIO_BRIDGE_PATH/bin    
export PYTHONPATH=\$PYTHONPATH:\$WIO_BRIDGE_PATH/hosts/python    
### WIDEIO-BRIDGE SECTION END
EOF
  fi
}


function uninstall_user_and_modify_bashrc() {
awk -v recopy=1 "
(/### WIDEIO-BRIDGE SECTION BEG/){recopy=0;}
(/### WIDEIO-BRIDGE SECTION END/){recopy=1;}
(!/### WIDEIO-BRIDGE SECTION END/){if (recopy) 
               print \$0;
       }
" < ~/.bashrc > /tmp/$USER-bashrc-edit || return -1
mv /tmp/$USER-bashrc-edit ~/.bashrc || return -1
echo "ok"
}

function create_dist() {
  touch .rm && find . -iname "*~" | xargs rm -f .rm
  touch .rm && find . -iname "*.pyc" | xargs rm -f .rm
  [ -f "../wideio-bridge-$VERSION.tgz" ] && echo "this distribution already exists" 1>&2 && exit -1
  cp  ../wideio-specifications/outputs/ALGOPACK.pdf docs
  cp  ../wideio-specifications/outputs/WIOPACKAGE.pdf docs
  cp  ../wideio-specifications/outputs/MIMEJSON.pdf docs
  cp  ../wideio-specifications/outputs/WDA.pdf docs
  cp  ../wideio-specifications/outputs/WRE.pdf docs
  CDX=$(basename $PWD)
  cd ..
  ln -s "$CDX" "wideio-bridge-$VERSION"
  find "wideio-bridge-$VERSION"/. -type f | grep -v "\\.git"  | xargs tar -czf "wideio-bridge-$VERSION.tgz" 
  rm  "wideio-bridge-$VERSION"
}

MODE="help"
if [ "$1" ]; then
  MODE=$1;
fi  


case $MODE in
  clean)
     echo "NYI"
     exit -1  
  ;;
  dist)
     echo " creating a distribution of WIDE IO BRIDGE" 1>&2
     echo "NYI"
     exit -1
  ;;
  prepare)
     git submodule update --init
     ln -sf ../../../wio-api/python/wideio/api hosts/python/wio/api
     for f in wio-bridge-mimejson/*; do
     ln -sf ../../../$f hosts/python/wio/
     done
     ln -sf ../../../../wideio-superglue hosts/python/wio/superglue
  ;;
  install_user)
    install_user_and_modify_bashrc
    ;;
  uninstall_user)
    uninstall_user_and_modify_bashrc
    ;;
  create_dist)
    create_dist
    ;;    
  help)
cat << EOF
$0 is used to install the WIDE IO bridge on your computer.

Available command :
  $0 install_user    - install the WIDE IO BRIDGE for your specific unix user
  $0 uninstall_user  - uninstall the WIDE IO BRIDGE for your specific unix user
  
EOF
    ;;
  *)
    echo "unsupported command" 2>&1
esac
