#!/usr/bin/env python2
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
#                           ..-.:.:...
#                        :.-- -     ..:...
#                    :.:. -             -.:...
#                :.:. -                     ..:...
#            :.:. .            _;:__.          ...-...
#        :.:. .               :;    -+_    -|       .--...
#    :.-- .                    -=      -~-.-           -.:...
#  -:...              ___.      -=_                        -.--
#  ...    .          =;  --=_     :=                   ..   ...
#  .-.      . .              ~-___=;               . -      .:.
#  ...           -.                             -.          ...
#  .:.                .                    . .              .:.
#  ...                   -.             -.                  ...
#  .:.                      ...    . -                -~4>  .:.
#  ...       _^+_.              -.                       2  ...
#  .:.           ~,              .                 /'   _(  .:.
#  ....           <              -          +'  ^LJ>   _^   ...
#  .:..          _);             -     _   J   _/  ~~-'    .:.
#  ....        _&i^i             .   _~_, <(   .^           ...
#   :.       _v>^  <             .  _X~'  -s,               .:.
#   :.             -=            .   S      ^'              ...
#   :....           -=_  ,       .   2                    ..-.:
#      :....          -^^        .                    .-.:. .
#         ..:...                 .                 ..:. -
#             -.:...             .           . :.-- -
#                  :....         .         -.-- .
#                     -.:...     .    ..:.: -
#                          -.:......--. -
#                              -.:. .
# 
# 
# Copyright (c) WIDE IO LTD
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of Wide.IO nor the names of others contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE WIDE.IO AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import os
import sys
import subprocess
import json
import glob
from wio.bridge import ptabparser as parser
from wio.bridge.exchange_files import local_input_file
from wio.bridge.exchange_files import local_output_file

try:
  from wio.bridge import publish_function
except Exception,e:
  if not int(os.environ.get("IGNORE_MISSING_WIO",0)):
    sys.stderr.write("Warning: wideio-bridge is not imported %r\n"%(e,))
  def publish_function(*args,**kwargs): return lambda x:x
try:
    # not used
    from wio.bridge import publish_function
except Exception, e:
    import os, sys
    if not int(os.environ.get("IGNORE_MISSING_WIO", 0)):
        sys.stderr.write("Warning: wideio-bridge is not imported %r\n" % (e,))

    def publish_function(*args, **kwargs):
        return lambda x: x




def get_datatype(moduletype):
  """ return a type stored in a python module"""
  moduletype=moduletype.split(".")
  cm=__builtins__
  cp=[]
  for m in moduletype[:-1]:
    cm=__import__(".".join(cp+[m]),fromlist=cp)
    cp.append(m)
  return getattr(cm,moduletype[-1])

# @publish_function()  # # << GeT EXTENDED DOC FROM MAN ?
# def run_program(input_data="1+1\n", shell_prg="bc", _WIO_REQUEST=None, _WIO_ADDRESS=None, _WIO_DATASET=None, **kwargs):
#     """
#     This is a simple code that runs a shell script - and returns its result.
#     """
#     # print _WIO_REQUEST
#     p = subprocess.Popen([shell_prg], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
#     out, err = p.communicate(input_data)
#     if err:
#         sys.stderr.write(err)
#     sys.stdout.write(out)


def validate_param(param_to_add, parameter):
    """
    This function is called to validate an input parameter, checks the datatype, whether it is within range,    
    whether the mimetype is correct.
    
    
    
    :param param_to_add: input parameter
    :param ptab: ptab file for the program
    :return: true if parameter was validated
    """
    success = True
    number_param = None  # get the number value of the param
    # try:  # if it even is a number
    if parameter.datatype == 'int':
        number_param = int(param_to_add)
    # except ValueError:
    # pass
    # try:
    if parameter.datatype == 'float':
        number_param = float(param_to_add)
    # except ValueError:
    #     pass
    if number_param is not None:  # if it is a number, validate
        if parameter.get_max() is not None and number_param > parameter.get_max():
            sys.stderr.write( param_to_add + " for " + parameter.name + " is too big (max: " + \
                  str(parameter.get_max()) + ")\n")
            success = False
        if parameter.get_min() is not None and number_param < parameter.get_min():
            sys.stderr.write(param_to_add + " for " + parameter.name + " is too small\n")
            success = False
    elif parameter.get_mimetype() == "":  # not a number, could be a file
        pass
    return success

def make_run_program_for_ptab(filename):
    """
    Defines new function for calling the program
    associated with it
    :param filename: filename of .ptab file
    :return: published run function
    """
    
    
    
    
    ptab = parser.parse_ptab(filename)

    def run_program2(input_data=None, shell_prg=ptab.command, WIO_REQUEST=None, _WIO_ADDRESS=None, _WIO_DATASET=None,
                     **kwargs):
        """
        Code that runs a shell script - and returns its result.
        """
        request = kwargs
        command = [shell_prg]
        if WIO_REQUEST and 'parameters' in WIO_REQUEST:
            request = WIO_REQUEST['parameters']
        _local_file = {
            'local_input_file': local_input_file,
            'local_output_file': local_output_file,
        }
        _std_ = {
            '%stdin%': ['local_input_file', subprocess.PIPE, None],
            '%stdout%': ['local_output_file', subprocess.PIPE, None],
            '%stderr%': ['local_output_file', subprocess.PIPE, None],
        }

        # sys.stderr.write('[ %r ] and %r before proc \n')%( shell_prg,_WIO_REQUEST)
        for std in _std_:
            if std in request:
                if type(request[std][0]) is dict:
                    _std_[std][2] = _local_file[_std_[std][0]](**request[std][0])
                else:
                    _std_[std][2] = _local_file[_std_[std][0]](request[std][0])
                _std_[std][1] = _std_[std][2].get_fd()
                del request[std]

        for index in range(0, len(ptab.parameters)):
            # loop through each parameter, and extract any input data from the request
            current_param, mandatory, parameter = ptab.parameters[index]
            success = True
            if not current_param in request:  # if the input data passed in does not contain the parameter,
                if parameter is None: # if not param it is a flag
                    if mandatory:
                        command.append(current_param)
                    continue
                else:
                    param_to_add = (parameter.get_default(), parameter.datatype)  # then go with default
                    if param_to_add[0] is None:  # cannot get a default value
                        if mandatory:
                            sys.stderr.write( "you must specify a value for " + current_param + ".\n")
                            sys.exit(-1)
                        continue
            else:  # otherwise use the one specified in command
                # request == tuple (value, type if type otherwise None)
                param_to_add = request[current_param]
                del request[current_param]
                if param_to_add[0] is True and parameter is None:  # we do not get value for it:
                    command.append(current_param)
                    continue
                success = validate_param(param_to_add[0], parameter)

            if success:
                #
                sys.stderr.write(str(current_param) + ": " + str(param_to_add) + "\n")
                #
                if param_to_add[1]:
                    parameter.datatype = param_to_add[1]
                param_to_add = param_to_add[0]
                if parameter.datatype in _local_file:
                    cl = _local_file[parameter.datatype]
                    if type(param_to_add) is dict:
                        param_to_add = cl(**param_to_add)
                    else:
                        param_to_add = cl(param_to_add)
                if param_to_add is not None:
                    if parameter.flag:
                        command.append(parameter.flag)
                    command.append(str(param_to_add))
            else:
                sys.stderr.write("parameter incorrect\n")
                sys.exit(-1)
        for unused_param in request:
            sys.stderr.write("parameter '%s' is not valid, continuing without\n" % unused_param)
        
        sys.stderr.write("%r %s"%( command, "after processing.\n"))
        p = subprocess.Popen(command,
                             stdin=_std_['%stdin%'][1],
                             stdout=_std_['%stdout%'][1],
                             stderr=_std_['%stderr%'][1])
        for std in _std_:
            if _std_[std][2]:
                _std_[std][2].close_fd()
        if input_data:
            out, err = p.communicate(input_data)
        else:
            out, err = p.communicate()
        if err:
            sys.stderr.write(err)
        sys.stderr.write(out)


    import wio.superglue.utils as su
    parameters={}
    # 'datatype', 'desc', 'flag', 'get_default', 'get_max', 'get_mimetype', 'get_min', 'name', 'obligatory', 'properties', 'set_desc', 'set_name', 'set_prop', 'set_type']
    
    ## OK WE WRAP THE SHELL PROGRAM INTO A PYTHON FUNCTION
    for p in ptab.parameters:
      sys.stderr.write("-- %r\n"%(p,))
      parameters[p[0]]=su.wideio_type(get_datatype(p[2].datatype),add_externals={'mandatory':p[1], 'description': p[2].desc}, try_construct=False,**p[2].properties)
    return publish_function(parameters=parameters, name=ptab.command)(run_program2)

# this is called two time ????
for x in glob.glob("*.ptab"):
    # parse each ptab and generate run functions for each program
    make_run_program_for_ptab(x)


if __name__ == "__main__":
    from wio.bridge import wideio_main
    a = wideio_main()

