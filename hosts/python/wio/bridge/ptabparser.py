# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
#                           ..-.:.:...
#                        :.-- -     ..:...
#                    :.:. -             -.:...
#                :.:. -                     ..:...
#            :.:. .            _;:__.          ...-...
#        :.:. .               :;    -+_    -|       .--...
#    :.-- .                    -=      -~-.-           -.:...
#  -:...              ___.      -=_                        -.--
#  ...    .          =;  --=_     :=                   ..   ...
#  .-.      . .              ~-___=;               . -      .:.
#  ...           -.                             -.          ...
#  .:.                .                    . .              .:.
#  ...                   -.             -.                  ...
#  .:.                      ...    . -                -~4>  .:.
#  ...       _^+_.              -.                       2  ...
#  .:.           ~,              .                 /'   _(  .:.
#  ....           <              -          +'  ^LJ>   _^   ...
#  .:..          _);             -     _   J   _/  ~~-'    .:.
#  ....        _&i^i             .   _~_, <(   .^           ...
#   :.       _v>^  <             .  _X~'  -s,               .:.
#   :.             -=            .   S      ^'              ...
#   :....           -=_  ,       .   2                    ..-.:
#      :....          -^^        .                    .-.:. .
#         ..:...                 .                 ..:. -
#             -.:...             .           . :.-- -
#                  :....         .         -.-- .
#                     -.:...     .    ..:.: -
#                          -.:......--. -
#                              -.:. .
# 
# 
# Copyright (c) WIDE IO LTD
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of Wide.IO nor the names of others contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE WIDE.IO AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import glob
import sys
import re

from wio.bridge import transductor, wjson


class Parameter:
    def __init__(self, name=None, desc=None, datatype=None,
                 properties=None, obligatory=True, flag=None):
        self.name = name
        self.desc = desc
        self.properties = properties
        self.datatype = datatype
        self.obligatory = obligatory
        self.flag = flag

    def set_name(self, n):
        self.name = n.strip()

    def set_desc(self, d):
        self.desc = d

    def set_prop(self, p):
        self.properties = p

    def set_type(self, t):
        self.datatype = t.strip()

    #def get_default(self):
        #if 'default' in self.properties:
            #return self.properties['default']
        #else:
            #return None

    #def get_mimetype(self):
        #if 'mimetype' in self.properties:
            #return self.properties['mimetype']
        #elif '$mimetype$' in self.properties:
            #return self.properties['$mimetype$']

    #def get_min(self):
        #if 'min_value' in self.properties:
            #return self.properties['min_value']
        #else:
            #return None

    #def get_max(self):
        #if 'max_value' in self.properties:
            #return self.properties['max_value']
        #else:
            #return None

    def __str__(self):
        return self.name


class PTABInfo:
    __r_order = re.compile("<[^<>]*>|\[[^\[\]<>]*\]|\[-+[a-zA-Z]+(?![^\]]*\])")
    __r_optional = re.compile("\[[^\[\]]+\]")
    __r_arg = re.compile('<[^<>]+>')

    def __init__(self):
        self.ld = ""  # long description
        self.cmdline = None
        self.description = None
        self.parameters = []
        self.command = None

    def _clean_order(self, s):
        s = s.strip()
        obg = True
        if s[0] == '<' and s[-1] == '>':
            s = s[1:-1].strip()
            obg = True
        if s[0] == '[' and s[-1] == ']':
            obg = False
            s = s[1:-1].strip()
        return (s, obg, None)

    def _set_optional_flag(self, name, f):
        for i in range(0, len(self.parameters)):
            a, b, c = self.parameters[i]
            if name == a:
                self.parameters[i] = (a, False, f)

    def set_cmd_line(self, c):
        self.cmdline = c.group(0).strip()
        cmdline = self.cmdline
        #print "xo" , self.parameters
        self.parameters = [self._clean_order(m) for m in
                           self.__r_order.findall(cmdline)]
        #print "xo" , self.parameters
        _optional = self.__r_optional.findall(cmdline)
        for i in range(0, len(_optional)):
            _optional[i] = _optional[i][1:-1]
            arg = self.__r_arg.findall(_optional[i])
            for o in arg:
                _optional[i] = _optional[i].replace(o, '')
            arg = [o[1:-1] for o in arg]
            for a in arg:
                self._set_optional_flag(a, _optional[i].strip())
        return ""

    def append_argument(self, arg):
        # self.arglist.append(arg)
        arg[3] = str(arg[3]).replace("'", "\"")
        try:
            setp = False
            for i in range(0, len(self.parameters)):
                if arg[0] == self.parameters[i][0]:
                    n, o, f = self.parameters[i]
                    arg[3] = wjson.loads(arg[3])
                    p = Parameter(*arg, obligatory=o, flag=f)
                    self.parameters[i] = (n, o, p)
                    setp = True
            if setp is False:
                sys.stderr.write('\033[93m[warning:ptab parser]\033[0m: ' +
                                 str(arg[0]) +
                                 ' does not exist in the command line.\n')
                p = None
        except Exception, e:
            sys.stderr.write('\033[93m[warning:ptab parser]\033[0m: ' +
                             str(self.command) + str(e.message) + str(arg)
                             + '.\n')

    def start_new_parameter(self, param):
        #print param.groups()
        self.unfinished_param_arr = [x.strip() for x in param.groups()]
        return ""

    def finish_new_parameter(self, param):
        #print param.groups()
        self.unfinished_param_arr.extend([x.strip() for x in param.groups()])
        self.append_argument(self.unfinished_param_arr)
        return ""

    def set_ld(self, ld):
        self.ld = ld.group(0)

    def append_ld(self, ld):
        self.ld += ld.group(0)
        return ""

    def get_cmd_line(self):
        return self.cmdline

    def get_ld(self):
        return self.ld


def create_ptab_transductor(pi):
    TRANSDUCTOR = {
        'a': [
            ('###', "", "ld"),              # long description
            ('#', "", "cl"),                # command line
            (r'(.[^|\n]*)\|',               # (r'([A-Za-z_\$][^|\n]*)\|',
             pi.start_new_parameter, "p")   # parameters
        ],
        'cl': [
            (r'([^\n]+)\n', pi.set_cmd_line, 'a', 're.M')
        ],
        'p': [
            (r'([^|\n]*)\|([^|\n]*)\|([^|\n]*)', pi.finish_new_parameter,
             'a', 're.M')
        ],
        'ld': [
            (r'([^\n]+)', pi.append_ld, None, 're.M')
        ]
    }
    return transductor.Transductor(TRANSDUCTOR)  # 512


def parse_ptab(filename):
    pi = PTABInfo()
    pi.command = filename.split('.')[0]
    t = create_ptab_transductor(pi)
    t.process(open(filename).read(), 512)
    # print "filename:", pi.prgname, "\nparams:", pi.arglist, \
    # "\ncmdline:", pi.get_cmd_line(), "\ndesc:", pi.get_ld()
    # for op in pi._optional:
    # check now if the command line correspond to parameters
    #print "CL", pi.get_cmd_line()
    #print "PRM", pi.parameters
    # print pi.arglist
    for op in pi.parameters:
        if not op[0].startswith('-') and type(op[2]) in (type(None), str):
            ## FIXME: Don't use color code like this directly.
            sys.stderr.write('\033[91m[error]\033[0m: \'' + str(op[0]) + '\' should be defined.\n')
            exit(-1)
    return pi


def auto():
    for filename in glob.glob("*.ptab"):
        parse_ptab(filename)
        # PTABInfo should now contain all information for this program

if __name__=="__main__":
  print parse_ptab(sys.argv[1])
