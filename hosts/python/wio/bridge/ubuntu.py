# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
#                           ..-.:.:...
#                        :.-- -     ..:...
#                    :.:. -             -.:...
#                :.:. -                     ..:...
#            :.:. .            _;:__.          ...-...
#        :.:. .               :;    -+_    -|       .--...
#    :.-- .                    -=      -~-.-           -.:...
#  -:...              ___.      -=_                        -.--
#  ...    .          =;  --=_     :=                   ..   ...
#  .-.      . .              ~-___=;               . -      .:.
#  ...           -.                             -.          ...
#  .:.                .                    . .              .:.
#  ...                   -.             -.                  ...
#  .:.                      ...    . -                -~4>  .:.
#  ...       _^+_.              -.                       2  ...
#  .:.           ~,              .                 /'   _(  .:.
#  ....           <              -          +'  ^LJ>   _^   ...
#  .:..          _);             -     _   J   _/  ~~-'    .:.
#  ....        _&i^i             .   _~_, <(   .^           ...
#   :.       _v>^  <             .  _X~'  -s,               .:.
#   :.             -=            .   S      ^'              ...
#   :....           -=_  ,       .   2                    ..-.:
#      :....          -^^        .                    .-.:. .
#         ..:...                 .                 ..:. -
#             -.:...             .           . :.-- -
#                  :....         .         -.-- .
#                     -.:...     .    ..:.: -
#                          -.:......--. -
#                              -.:. .
# 
# 
# Copyright (c) WIDE IO LTD
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of Wide.IO nor the names of others contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE WIDE.IO AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
##

import sys
import subprocess  


COMPATIBLE_SYSTEM=True

try:
  import apt
  cache=apt.Cache()
except:
  COMPATIBLE_SYSTEM=False

def is_compatible_system():
  return COMPATIBLE_SYSTEM

def apt_dep_name(d):
  if type(d) == str:
    return d
  return d.name.split(":")[0]

def get_default_apt_package(pkgname):  
    if type(pkgname) not in [str, unicode]:
      pkgname=apt_dep_name(pkgname)
    try:
       return cache[pkgname].versions[0]
    except Exception,e:
      sys.stderr.write(repr(e)+"\n")
      return None
      
  

def get_apt_dependencies(pkgname):
  try:  
    deps=get_default_apt_package(pkgname).dependencies
    # FIXME:  CHECK IT ALWAYS WORKS  
    return reduce(lambda a,b:a+b,deps,[])
  except Exception,e:
    sys.stderr.write(repr(e)+"\n")
    return []




## useful for rmount
def _get_apt_rdependencies(pkgname,level=0):
  rdeps={pkgname:[]}
  nd=[pkgname]
  while len(nd):
    tnd=nd
    nd=[]
    for cp in tnd:
     for a in get_apt_dependencies(cp):
       an=apt_dep_name(a)
       if not an in rdeps:
         rdeps[an]=[apt_dep_name(cp)]
         nd.append(a)
       else:
         rdeps[an].append(apt_dep_name(cp))
  return rdeps


max_h_cache={}
def max_h(b,r,s=[]):
  if b in max_h_cache:
    return max_h_cache[b]
  if (b not in r):
    max_h_cache[b]=0
    return 0
  if (b in r[b]):
    sys.stderr.write("warning %r : circular dep\n"%(b))
    max_h_cache[b]=0
    return 0 
  #print b,
  if b in s:    
    max_h_cache[b]=0
    return 0
  if (len(r[b])==0):
    max_h_cache[b]=0
    return 0
  max_h_cache[b]=max(map(lambda x:max_h(x,r,s+[b])+1 , r[b]))
  return max_h_cache[b]
    

def get_apt_rdependencies(*args,**kwargs):
  f=_get_apt_rdependencies(*args,**kwargs)
  b={}
  for i in f.items():
    for k in i[1]:
      if k in b:
        b[k].append(i[0])
      else:
        b[k]=[i[0]]
  #print b
  h={}
  for x in b.keys():
    h[x]=max_h(x,b)
  return sorted(h.items(),key=lambda x:x[1])
