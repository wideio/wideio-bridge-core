# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
#                           ..-.:.:...
#                        :.-- -     ..:...
#                    :.:. -             -.:...
#                :.:. -                     ..:...
#            :.:. .            _;:__.          ...-...
#        :.:. .               :;    -+_    -|       .--...
#    :.-- .                    -=      -~-.-           -.:...
#  -:...              ___.      -=_                        -.--
#  ...    .          =;  --=_     :=                   ..   ...
#  .-.      . .              ~-___=;               . -      .:.
#  ...           -.                             -.          ...
#  .:.                .                    . .              .:.
#  ...                   -.             -.                  ...
#  .:.                      ...    . -                -~4>  .:.
#  ...       _^+_.              -.                       2  ...
#  .:.           ~,              .                 /'   _(  .:.
#  ....           <              -          +'  ^LJ>   _^   ...
#  .:..          _);             -     _   J   _/  ~~-'    .:.
#  ....        _&i^i             .   _~_, <(   .^           ...
#   :.       _v>^  <             .  _X~'  -s,               .:.
#   :.             -=            .   S      ^'              ...
#   :....           -=_  ,       .   2                    ..-.:
#      :....          -^^        .                    .-.:. .
#         ..:...                 .                 ..:. -
#             -.:...             .           . :.-- -
#                  :....         .         -.-- .
#                     -.:...     .    ..:.: -
#                          -.:......--. -
#                              -.:. .
# 
# 
# Copyright (c) WIDE IO LTD
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of Wide.IO nor the names of others contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE WIDE.IO AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
##
# THE CLASSES ARE USED TO COMMUNICATE PARAMETERS WITH PROGRAMS REQUESTING
# THEIR ARGUMENTS AS FILES
# IF THE IMPUT IS ALREADY A FILE THEN IT CAN OBVIOUSLY BE MAINTAINED AS A
# FILE OTHER WISE IT BECOMES
# A GOOD TYPED CANDIDATE TO APPLY SUPERGLUE LOGIC IN PYTHON
##

# replace random by uuid
import uuid
import os
# import base64
# import time
# import random


# range should not take the 255 ascii but between 60-71,101-132, 141-172
#                                                 [0-9] [A-Z]    [a-z]
# def random_filename(prefix="f-", suffix=".dat", lens=16):
#     return prefix+base64.b64encode(reduce(lambda a,b:a
#                                           +chr(random.randrange(0,255)),
#                                           range(lens), "")+"-"+ \
#                                    str(time.time()))+suffix


class local_input_file(object):
    def __init__(self, filename=None, content=None,
                 mimetype="application/bytes", **kwargs):
        # ## Create or mention a file content through a file
        # to pass parameters to a program
        assert(content or filename)
        self.content = content
        self.filename = filename
        self.mimetype = mimetype
        self.fd = None
        self.created = False
        if filename is not None:
            self.created = True
            if not os.path.exists(filename):
                print '[error] :', filename, 'does not exists'
                exit(-1)

    def read(self, **kwargs):
        with open(self.filename) as o:
            self.content = o.read(**kwargs)
            return self.content

    def __unicode__(self):
        return self.filename

    def __str__(self):
        return self.filename

    def __del__(self):
        self.close_fd()
        if self.created:
            os.remove(self.filename)
            self.created = False

    def close_fd(self):
        if self.fd:
            os.close(self.fd)
            self.fd = None

    def get_fd(self):
        if self.filename is None:
            self.filename = str(uuid.uuid4())
            with open(self.filename, 'w') as o:
                o.write(self.content)
            self.created = True
        self.fd = os.open(self.filename, os.O_RDONLY)
        return self.fd
#             os.remove(self.filename)


# class local_input_file(object):
#     def __init__(self, filename=None, filecontent=None,
#                  mimetype="application/bytes", origfilename=None):
#         ### Create or mention a file content through a file
#             to pass parameters to a program
#         # TODO: allow fd usage
#         if filename is None:
#             if origfilename:
#                 filename=os.basename(filename)
#             else:
#                 filename=random_filename()

#         self.filename=filename
#         self.mimetype=mimetype
#         self.created=False
#         if (filecontent):
#             with open(filename,"wb") as o:
#                 o.write(filecontent)
#                 o.close()
#             self.created=True
#         else:
#             if origfilename!=None:
#                 if origfilename!=filename:
#                     os.symlink(origfilename,filename)
#                     self.created=True

#         # wrong this should be filename right ?
#         # assert(os.path.exists(filecontent))
#         assert(os.path.exists(filename))

#     def __unicode__(self):
#         return self.filename

#     def __str__(self):
#         return self.filename

#     def __del__(self):
#         if self.created:
#             os.remove(self.filename)


class local_output_file(object):
    def __init__(self, filename=None, extension=".dat",
                 mimetype="application/bytes", **kwargs):
        # Create or mention a file content through a file
        # to pass parameters to a program
        if filename is None:
            filename = str(uuid.uuid4()) + extension
        if os.path.exists(filename):
            print '[error] :', filename, 'already exists'
            exit(-1)
        self.filename = filename
        self.mimetype = mimetype
        self.fd = None
        self.created = False

    def write(self, content):
        with open(self.filename, 'w') as o:
            o.write(content)

    def __unicode__(self):
        return self.filename

    def __str__(self):
        return self.filename

    def __del__(self):
        self.close_fd()
        if self.created:
            os.remove(self.filename)
            self.created = False

    def get_fd(self):
        self.fd = os.open(self.filename, os.O_WRONLY | os.O_CREAT)
        self.created = True
        return self.fd

    def close_fd(self):
        if self.fd is not None:
            os.close(self.fd)
            self.fd = None

# class local_output_file(object):
#     def __init__(self,filename=None, extension=".dat",
#                  mimetype="application/bytes"):
#         ### Create or mention a file content through a file to
#         ### pass parameters to a program
#         if filename==None:
#             filename=random_filename(suffix=extension)
#         self.filename=filename
#         self.mimetype=mimetype

#     def __unicode__(self):
#         return self.filename

#     def __str__(self):
#         return self.filename

#     def __del__(self):
#         if self.created:
#             os.remove(self.filename)
