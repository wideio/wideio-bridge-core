# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
#                           ..-.:.:...
#                        :.-- -     ..:...
#                    :.:. -             -.:...
#                :.:. -                     ..:...
#            :.:. .            _;:__.          ...-...
#        :.:. .               :;    -+_    -|       .--...
#    :.-- .                    -=      -~-.-           -.:...
#  -:...              ___.      -=_                        -.--
#  ...    .          =;  --=_     :=                   ..   ...
#  .-.      . .              ~-___=;               . -      .:.
#  ...           -.                             -.          ...
#  .:.                .                    . .              .:.
#  ...                   -.             -.                  ...
#  .:.                      ...    . -                -~4>  .:.
#  ...       _^+_.              -.                       2  ...
#  .:.           ~,              .                 /'   _(  .:.
#  ....           <              -          +'  ^LJ>   _^   ...
#  .:..          _);             -     _   J   _/  ~~-'    .:.
#  ....        _&i^i             .   _~_, <(   .^           ...
#   :.       _v>^  <             .  _X~'  -s,               .:.
#   :.             -=            .   S      ^'              ...
#   :....           -=_  ,       .   2                    ..-.:
#      :....          -^^        .                    .-.:. .
#         ..:...                 .                 ..:. -
#             -.:...             .           . :.-- -
#                  :....         .         -.-- .
#                     -.:...     .    ..:.: -
#                          -.:......--. -
#                              -.:. .
# 
# 
# Copyright (c) WIDE IO LTD
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of Wide.IO nor the names of others contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE WIDE.IO AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import sys

class Transductor(object):
  def __init__(self, table, state0=None):
     self.table=table
     self.state0=(state0==None) and  ( 'a' if table.has_key('a') else table.keys()[0]  )  or state0
     self.statestack=[]
  def process(self,t,maxread=12):
     import re
     lt=len(t)
     p=0
     output=u""
     state=self.state0
     #print "state0", state
     self.tablec={}
     for xstate in self.table.keys():
       self.tablec[xstate]=map(lambda r:(r,re.compile(r[0],re.U)) if len(r)<4 else (r,re.compile(r[0],re.U  | eval(r[3]))) ,self.table[xstate])
     while (p<lt):
       #print state,
       for rr in self.tablec[state]:
         #m=re.match(u"^"+r[0],t[p:],re.U) # re.M
         r=rr[0]
         m=rr[1].match(t,p,p+maxread)
         if m:
            #print m.group(0)
            break
            
       if (not m):#1365
         #if (type(output)!=unicode):
         #  sys.stderr.write("output type is wrong\n")
         #  assert(False)
         #if (type(t[p])!=unicode):
         #  sys.stderr.write("t[p] type is wrong\n")
         #  assert(False)
         output+=t[p]
         p+=1
       #assert(m!=None)
       else:
         grps=m.groups()
         if (type(r[1]) in [ str, unicode ] ):
            sx=r[1]
         elif (callable(r[1])):
            sx=r[1](m)   
         else:
            sx=m.group(0)
         #print "bbb",sx            
         lx=len(m.group(0))
         #try:
         #  for i in range(len(grps)):
         #    #print (u"\\"+unicode(i),grps[i],sx)             
         #    sx=re.subn(u"\\"+unicode(i),grps[i],sx)[0]
         #except:
         #   sys.stderr.write("strange bug in substitution")
         #print "sx",sx
         output+=sx
         #print r
         if (len(r)>2):
           if (r[2]!=None):
             for targetstate in str(r[2]).split(','):
               if targetstate=="#push": 
                  #print state,"push", self.statestack
                  self.statestack.append(state)
               elif targetstate=="#pop":
                  try:
                    #print state,"pop", self.statestack
                    state=self.statestack.pop(-1)
                  except:
                    sys.stderr.write("Transductor warning : POP from empty list \n")
               else:
                  #print "state switch", state, targetstate
                  state=targetstate 
         assert(lx>0)
         p+=lx
     return output


try:
  raise Exception, "Not Finished"
  ##
  ## Actually ply is based on a big regular expression, and on the use of 
  ##    group_dict
  ##
  import ply.lex as lex 
  
  class PlyTransducor:
    def build_one_state_lexer(self,it):
       rules=it[1]
       class MyLexer(object):
           def build(self, **kwargs):
             self.lexer =lex.lex(object=self, ** kwargs)
       MyLexer.rules=rules
       for ri in range(len(rules)):
          if type(rules[ri]) in [unicode, str]:
            def fct(selfstate,x):
              rules[ri][0]
              return rules[ri][1]
            exec "MyLexer.t_R%08d=fct" in {'MyLexer':MyLexer, 'fct':fct}
          else:
            def fct(selfstate,x):
              rules[ri][0]
              return rules[ri][1](x)            
            exec "MyLexer.t_R%08d=fct" in {'MyLexer':MyLexer, 'fct':fct}
       return MyLexer
       
    def __init__(self, table, state0=None):
       self.table=table
       self.lexertable=dict(map(self.build_lexer,table.items()))
       self.state0=(state0==None) and  ( 'a' if table.has_key('a') else table.keys()[0]  )  or state0
       self.statestack=[]
    def process(self,t,maxread=12):
       pass
except:
  pass

import os,ctypes

def encrepl(x):
   return x.encode('utf8').replace('\\','\\\\').replace('\t','\\t').replace('\n','\\n').replace('"','\\"')

def encmatch(x):
   return x.encode('utf8').replace('\\','\\\\').replace('\t','\\t').replace('\n','\\n').replace('"','\\"').replace("{","\\{").replace("}","\\}")

def cfunc(name, dll, result, *args):
    '''build and apply a ctypes prototype complete with parameter flags
    e.g.
cvMinMaxLoc = cfunc('cvMinMaxLoc', _cxDLL, None,
                    ('image', POINTER(IplImage), 1),
                    ('min_val', POINTER(double), 2),
                    ('max_val', POINTER(double), 2),
                    ('min_loc', POINTER(CvPoint), 2),
                    ('max_loc', POINTER(CvPoint), 2),
                    ('mask', POINTER(IplImage), 1, None))
means locate cvMinMaxLoc in dll _cxDLL, it returns nothing.
The first argument is an input image. The next 4 arguments are output, and the last argument is
input with an optional value. A typical call might look like:

min_val,max_val,min_loc,max_loc = cvMinMaxLoc(img)
    '''
    atypes = []
    aflags = []
    for arg in args:
        atypes.append(arg[1])
        aflags.append((arg[2], arg[0]) + arg[3:])
    return ctypes.CFUNCTYPE(result, *atypes)((name, dll), tuple(aflags))


class CompiledTransductor:
  def __init__(self, table, state0=None):
     import ctypes
     import sys
     import hashlib
     self.table=table
     self.state0=(state0==None) and  table.keys()[0] or state0
     states=table.keys()
     for x in states:
        if not x.isalnum():
          raise Exception,"Transductor states must be alpha numeric strings"
     try:
       os.stat(os.environ["HOME"]+"/.mml")
     except:
       sys.stderr.write("Created directory for Transductors cache:"+ os.environ["HOME"]+"/.mml")
       os.mkdir(os.environ["HOME"]+"/.mml")
     hk=hashlib.md5(unicode(table).encode('utf8')).hexdigest()
     sofile=os.environ["HOME"]+"/.mml/transductor-%s.so"%(hk,)
     try:
       os.stat(sofile)
     except:
       lfile=os.environ["HOME"]+"/.mml/transductor-%s.l"%(hk,)
       cfile=os.environ["HOME"]+"/.mml/transductor-%s.c"%(hk,)
       f=file(lfile,"wb") 
       f.write("%{\n#include<stdlib.h>\n#include<stdio.h>\nvoid putbuf(char * c);\n%}\n\n")
       for i in states:
         f.write("%%section state%s\n"%(i,))
       f.write("\n%%\n\n")
       for i in states:
         for r in table[i]:
           #print r
           if (r[0]) and (len(r)==3) and r[1] and r[2]: 
             f.write("<state%s>%s {putbuf(\"%s\"); BEGIN(state%s);}\n"%(i,encmatch(r[0]),encrepl(r[1]),r[2]))
           elif (r[0]) and (len(r)==3) and not r[1] and r[2]:
             f.write("<state%s>%s {putbuf(yytext); BEGIN(state%s);}\n"%(i,encmatch(r[0]),r[2]))
           elif (r[0]) and ((len(r)==2) and r[1]) or (len(r)==3):
             f.write("<state%s>%s {putbuf(\"%s\");}\n"%(i,encmatch(r[0]),encrepl(r[1])))
           elif (r[0]) and ((len(r)==2) and not r[1]) or (len(r)==1):
             f.write("<state%s>%s {putbuf(yytext);}\n"%(i,encmatch(r[0])))
           else:
             raise Exception, "Unknown type of rule in transductor"
         f.write("<state%s>. {putbuf(yytext);}\n"%(i,))
	 f.write("<state%s>[\\n] {putbuf(yytext);}\n"%(i,))
       f.write("%%\n\n")
       f.write("""
       static char * curbuf=NULL;
       static int lbuf=0;
       static int cbuf=0;
       
       void initbuf() {
         lbuf=1024;
	 cbuf=0;
	 curbuf=(char*)calloc(1024,1);
       }
       
       void doublebuf() {
         char * rbuf;
	 
         lbuf*=2;
	 rbuf=(char*)malloc(lbuf);
	 memcpy(rbuf,curbuf,cbuf);
	 free(curbuf);
	 curbuf=rbuf;
       }

       
       void putbuf(char * c) {
         int l;
	 
         l=strlen(c);
	 while (cbuf+l>=lbuf) {
	   doublebuf();
	 }
         memcpy(curbuf+cbuf,c,l);
	 cbuf+=l;
       }
       
       
       void destroybuf() {
          free(curbuf);
	  lbuf=0;
	  cbuf=0;
       }
       
       char * clex(char * text) {
         initbuf();
	 yy_scan_string(text);
	 BEGIN(state"""+self.state0+""");
	 yylex();
	 /*printf("\\ninit text=%s\\nlen =%d \\n result = %s\\n",text,cbuf,curbuf);*/
	 return curbuf;
       }
       
       
       
       """)
       f.close()
       os.system("flex -o%s %s"%(cfile,lfile))
       os.system("gcc -O2 -shared -fPIC -o%s %s -lfl"%(sofile,cfile))
     self.lexer=ctypes.CDLL(sofile)
     clex=cfunc("clex",self.lexer,ctypes.c_char_p,("xxx",ctypes.c_char_p,1))
     destroybuf=cfunc("destroybuf",self.lexer,None)
     def pylex(s):
       l=clex(s)
       l=l[:]
       destroybuf()
       return l
     self.pylex=pylex
   
  def process(self,t,maxread=-1):
       return self.pylex(t.encode('utf8')).decode('utf8')
  
import hashlib
  
class CachedTransductor(Transductor):
  transductor_cache=os.environ["HOME"]+"/.mml"
  def __init__(self,table,*args,**kwargs):
    super(CachedTransductor,self).__init__(table,*args,**kwargs)
    self.tablehash=hashlib.md5(unicode(self.table).encode('utf8')).hexdigest()
  def process(self,x,*args,**kwargs):
    texthash=hashlib.md5(unicode(x).encode('utf8')).hexdigest()
    transductor_cachefile=("%s/%s-%s.tmp")%(self.transductor_cache,self.tablehash,texthash)
    try: 
      os.stat(transductor_cachefile)
      return file(transductor_cachefile).read().decode('utf8')
    except:
      #sys.exit(0)
      s=super(CachedTransductor,self)
      r=s.process(x,*args,**kwargs)
      file(transductor_cachefile,"w").write(r.encode('utf8'))
      return r
