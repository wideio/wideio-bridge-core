# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
#                           ..-.:.:...
#                        :.-- -     ..:...
#                    :.:. -             -.:...
#                :.:. -                     ..:...
#            :.:. .            _;:__.          ...-...
#        :.:. .               :;    -+_    -|       .--...
#    :.-- .                    -=      -~-.-           -.:...
#  -:...              ___.      -=_                        -.--
#  ...    .          =;  --=_     :=                   ..   ...
#  .-.      . .              ~-___=;               . -      .:.
#  ...           -.                             -.          ...
#  .:.                .                    . .              .:.
#  ...                   -.             -.                  ...
#  .:.                      ...    . -                -~4>  .:.
#  ...       _^+_.              -.                       2  ...
#  .:.           ~,              .                 /'   _(  .:.
#  ....           <              -          +'  ^LJ>   _^   ...
#  .:..          _);             -     _   J   _/  ~~-'    .:.
#  ....        _&i^i             .   _~_, <(   .^           ...
#   :.       _v>^  <             .  _X~'  -s,               .:.
#   :.             -=            .   S      ^'              ...
#   :....           -=_  ,       .   2                    ..-.:
#      :....          -^^        .                    .-.:. .
#         ..:...                 .                 ..:. -
#             -.:...             .           . :.-- -
#                  :....         .         -.-- .
#                     -.:...     .    ..:.: -
#                          -.:......--. -
#                              -.:. .
# 
# 
# Copyright (c) WIDE IO LTD
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of Wide.IO nor the names of others contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE WIDE.IO AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

### WIDE IO BRIDGE FOR PYTHON - THIS ALLOWS TO RUN PYTHON CODE IN "UNIX MODE" 
### (i.e. running a process for each input)
### FOR WIDEIO BRIDGE

import json, sys, os, socket, argparse, glob, inspect



try:
  import bson
  from bson import Binary, Code
  from bson.json_util import dumps, loads
except:
  sys.stderr.write("[WARNING] bson not installed on this host - "
                   +"bson serialisation won't be supported\n")

  
### ------------------------------------------------------------------------
### FIXME: the output of the introspection should be cached to speed up
### execution of the program in most cases.
### ------------------------------------------------------------------------
__wideio__function_register={}
__wideio__dataset_register={}
__wideio__metric_register={}
__wideio__superglue_datatypes_register={}
__wideio__superglue_dataconverters_register={}



def __wideio__validate_function(f):
    return True


### ------------------------------------------------------------------------
### THIS REGISTERS A FUNCTION IN THE FUNCTION REGISTER
### ------------------------------------------------------------------------
def publish_function(name=None,parameters=None,binputs=None, boutputs=None, 
                     description=None, authors=None,**kwargs):
  def dec(f):
    global __wideio__function_register
    spec = inspect.getargspec(f)
    inputs=binputs
    outputs=boutputs
    if "_WIO_REQUEST" in spec.args:#f.func_code.co_varnames[:f.func_code.co_argcount]:
        f.__wio_call_mode__=1
    else:
        f.__wio_call_mode__=0
    wid=(name if name else f.__name__)
    cp=parameters
    if (cp==None) and len( spec.args )== len( spec.defaults):
      cp=[]
      if inputs==None:
        inputs=[]
      if outputs==None:
        outputs=[]
      for a in filter(lambda x:x[0][0]!='_',zip(spec.args,spec.defaults)):
        from wio.superglue import utils as su
        cp.append((a[0],su.wideio_typeof(a[1],add_externals={'default':a[1]})))
    __wideio__validate_function(f)
    f.__wideio_inputs=inputs
    f.__wideio_outputs=outputs
    f.__wideio_description=description
    f.__wideio_authors=authors
    f.__wideio_parameters=cp
    f.__wideio_name=wid
    f.__wideio_published=True
    if ((cp==None) and (len(spec.args))):
      sys.stderr.write("[warning] function published but parameters not "
      +"declared. please add parameters= as part of the publish function...")
    __wideio__function_register[wid]=f
    return f
  return dec




def __wideio__validate_dataset(c):
    L=0
    ### Ensure mandatory functions are there

    ###
    ### SUPPORT BASIC REFLEXIVITY 
    ###
    if not hasattr(c,"GetKeyDatatypeDescriptor"):
        raise ValueError("dataset not implemented as it should ")    
    if not hasattr(c,"GetValueDatatypeDescriptor"):
        raise ValueError("dataset not implemented as it should ")
    
    ### ITERATE ROWS
    ### Support fot iteration.
    if not hasattr(c,"GetElementByKey"):
        raise ValueError("dataset not implemented as it should ")
    if not hasattr(c,"GetNewKeyIterator"):
        raise ValueError("dataset not implemented as it should ")    
    if not hasattr(c,"KeyIteratorNext"):
        raise ValueError("dataset not implemented as it should ")
    
    
    
    ### ITERATE OTHER COLUMNS
    #This list the datasets known to share the same key space.
    #This list is not exhaustive
    if not hasattr(c,"ListRelatedDatasets"):
        raise ValueError("dataset not implemented as it should ")

    
    # Property are readonly attributes about the dataset
    if not hasattr(c,"ListProperties"):
        raise ValueError("dataset not implemented as it should ")        
    if not hasattr(c,"GetProperty"):
        raise ValueError("dataset not implemented as it should ")
    
    # Return potentially an indication on the number of elements in the dataset. Note that this value may be approximative
    # Check "CountAccuracy" Property
    if not hasattr(c,"GetCount"):
        raise ValueError("dataset not implemented as it should ")    
    
    ### Ok if all this is implemented this is a level 1 dataset.
    L=1
    

    ### Evaluate which optional functions are there
    if not hasattr(c,"GetNewBatchIterator"):
        return L
    if not hasattr(c,"BatchIteratorNext"):
        return L
    if not hasattr(c,"GetElementBatch"):
        return L
    L=3
    return L



def publish_dataset(name=None,parameters=None,binputs=None, boutputs=None, description=None, authors=None,**kwargs):
  """
  WIDE IO datasets are the continuation / extensions of pycvf datasets.
  They are conceptually similar to Hadoop RDDs.
  It is important to understand that datasets are immutable.
  """
  def dec(c):
    global __wideio__function_register
    __wideio__validate_dataset(c) 
    wid=(name if name else c.__name__)    
    __wideio__dataset_register[wid]=(c)
    c.__wideio_published=True    
    return c
  return dec




def publish_objective_metric(name=None,parameters=None,binputs=None, boutputs=None, description=None, authors=None,**kwargs):
  """
  A metric is a function that requires a dataset, check the existence of an existence of associated datasets providing objective 
  reference information or related computations, use a distance and a weighting to provide results on a specific dataset.
  """
  def dec(c):
    global __wideio__function_register
    __wideio__validate_objective_metric(c) 
    wid=(name if name else c.__name__)    
    __wideio__objective_metric_register[wid]=(c)
    return f
  return dec




### There are different ways to wrap / calls functions
### It essentially depends on how much the author of the function
### was aware/wanting to be aware that he was in a framework.
###

def _call_wideio_function0(f,request):
  return f(**request["parameters"])

def _call_wideio_function1(f,request):
  return f(WIO_REQUEST=request, **request["parameters"])

def _call_wideio_function2(c,request):
  if hasattr(c,"set_parameters"):
      c.set_parameters(**request["parameters"])
  c.set_request(request)
  return c.process_element(**request.get("inputs",{}))


_call_wideio_functions=[
  _call_wideio_function0,
  _call_wideio_function1,
  _call_wideio_function2,
]

def call_wideio_function(f,request):
  return _call_wideio_functions[f.__wio_call_mode__](f,request)






###
### Nodes are objects algorithms
###
def publish_node(*args,**kwargs):
  def dec(c):
    global __wideio__function_register
    def f(*args,**kwargs):
        return c.process_element(*args,**kwargs)
    if ["_WIO_REQUEST"] in f.func_code.co_varnames[:f.func_code.co_argcount]:
        f.__wio_call_mode__=1
        c.__wio_call_mode__=2
    else:
        f.__wio_call_mode__=0
        c.__wio_call_mode__=2
    wid=c.__name__
    c.__wideio_name=wid
    __wideio__function_register[wid]=c
    return c
  return dec

def update_with_type_conversion(d,p):
    ## FIXME: NOT YET IMPLEMENTED
    #d.update(p)
    d.update(dict(map(lambda x: (x[0],x[1][0]),p.items())))


##
## THE TCP SERVER MODE ALLOWS PROGRAM THAT ARE GOING TO BE CALLED OFTEN TO BE LOADED
##
## FIXME: This code is quite old it certainly needs to be updated.
def run_tcp_server(s,function):
    server_running=True
    while server_running:
       amount=decode_int(s.recv(4))
       request=bson.BSON.load(s.recv(amount))
       request["parameters"]={}
       update_with_type_conversion(request["parameters"], __override_params)
       if request["type"]=="compute":
           wideio_data = call_wideio_function(function,request)
           reply={"res":wideio_data}
       elif request["type"]=="stop_server":
           server_running=False
           reply={"res":"ok"}
       else:
           reply={"res":"error", "error":"unsupported request type : "+ request["type"]}
       reply=bson.BSON.encode(reply)
       s.send(encode_int(len(reply)))
       s.send(reply)

## ----------------------------------------------------------------------------------
## Declaration of the different running mode for the algorithms
## This defines how we connect the algorithms with its inputs / outputs
## ----------------------------------------------------------------------------------

WIDE_IO_MODES={}
def wideio_main_request_mode(OPT,function):
        """
        In this mode,  most parameters are passedthrough the request file
        """
        if OPT["wideio_serialisation"]=="bson":
            if not os.path.exists(os.path.join('request', 'request.bson')):
                raise RuntimeError('cannot find request file')
            request=bson.BSON(open("request/request.bson").read()).decode()
            wideio_data = call_wideio_function(function,request)
        elif OPT["wideio_serialisation"]=="json":
            if not os.path.exists(os.path.join('request', 'request.json')):
                raise RuntimeError('cannot find request file')
            request=json.loads(open("request/request.json").read())

            wideio_data = call_wideio_function(function,request)
        elif OPT["wideio_serialisation"]=="plain":
            plain=os.path.join('request', 'request.txt')
            if not os.path.exists(plain):
                raise RuntimeError('cannot find request file')
            request={}
            with open(plain, 'r') as f:
                # or,
                # wideio_data = dict([line.split() for line in f])
                # if you wish to be *terse* ...
                for line in f:
                    key, val = line.split()
                    request[key]=val

            update_with_type_conversion(request["parameters"],__override_params)
            wideio_data = call_wideio_function(function,request)
        else:
            sys.stderr.write("Unsupported serialization\n")
            exit(-1)
            wideio_data=None
        if wideio_data != None:
            if OPT["wideio_deserialisation"]=="json":
                # os.path ...
                with open('request/output.json', 'w') as outfile:
                    outfile.write(json.dumps(wideio_data))
                # we can stick in some json here ...
            else:
                pass
WIDE_IO_MODES["request"]=wideio_main_request_mode





def wideio_main_cli_mode(OPT,function):
        """
        In this mode,  input parameters are passed through the command line
        """    
        request={}
        request["parameters"]={}
        update_with_type_conversion(request["parameters"],__override_params)
        ## FIXME : THIS IS DRAFT
        request["inputs"]={}
        update_with_type_conversion(request["inputs"], __override_params) # ss
        wideio_data = call_wideio_function(function,request)
        if wideio_data != None:
            if OPT["wideio_deserialisation"]=="json":
                # os.path ...
                with open('request/output.json', 'w') as outfile:
                    outfile.write(json.dumps(wideio_data))
                # we can stick in some json here ...
            else:
                pass
WIDE_IO_MODES["cli"]=wideio_main_cli_mode









def wideio_main_server_mode(OPT,function):
    if OPT["wideio_mode"]=="server":
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect(("wio-000.home",int(OPT["wideio_port"])))  #was "127.0.0.1"
        run_server(s,function)
WIDE_IO_MODES["server"]=wideio_main_server_mode



## ----------------------------------------------------------------------------------
## Main fct and argument parsing
## ----------------------------------------------------------------------------------
def __show_help_mesg(oa, OPT,OPTIONS):
    sys.stderr.write("%s command line arguments\n"%(sys.argv[0],))
    for o in OPTIONS:
        sys.stderr.write("\t--%s <value>\t%s (%r)\n"%(o[0],o[2],o[1]))
    exit(0)

def __define_in_request(oa,OPT,OPTIONS):
   __override_params.update(json.loads("{%s}"%( oa[1],)))
   return 2

__override_params={}

_COMMANDS={}
def _wio_command(help_text=None):
  def dec(f):
    f.wideio_help_text=help_text
    _COMMANDS[f.__name__[4:]]=f
    return f
  return dec

@_wio_command()
def cmd_run(OPT,function):
      return WIDE_IO_MODES[OPT["wideio_mode"]](OPT,function)

@_wio_command()
def cmd_list_functions(OPT,function):
      for f in __wideio__function_register.items():
            print (f[0],{
                     'parameters':f[1].__wideio_parameters,
                     'inputs':f[1].__wideio_inputs,
                     'outputs':f[1].__wideio_outputs,
                     'description':f[1].__wideio_description,
                     'authors':f[1].__wideio_authors,
                     })
                     
@_wio_command()
def cmd_list_datasets(OPT,function):
      for f in __wideio__dataset_register.items():
            print (f[0],{
                     'parameters':f[1].__wideio_parameters,
                     'inputs':f[1].__wideio_inputs,
                     'outputs':f[1].__wideio_outputs,
                     'description':f[1].__wideio_description,
                     'authors':f[1].__wideio_authors,
                     })
                     
@_wio_command()
def cmd_list_metrics(OPT,function):
      for f in __wideio__metric_register.items():
            print (f[0],{
                     'parameters':f[1].__wideio_parameters,
                     'inputs':f[1].__wideio_inputs,
                     'outputs':f[1].__wideio_outputs,
                     'description':f[1].__wideio_description,
                     'authors':f[1].__wideio_authors,
                     })        
                     

@_wio_command()
def cmd_list_superglue_typedef(OPT,function):
      for f in __wideio__superglue_typedef_register.items():
            print (f[0],{
                     'parameters':f[1].__wideio_parameters,
                     'inputs':f[1].__wideio_inputs,
                     'outputs':f[1].__wideio_outputs,
                     'description':f[1].__wideio_description,
                     'authors':f[1].__wideio_authors,
                     })        
                     
                     
                     
@_wio_command()
def cmd_list_superglue_typeconv(OPT,function):
      for f in __wideio__superglue_typeconv_register.items():
            print (f[0],{
                     'parameters':f[1].__wideio_parameters,
                     'inputs':f[1].__wideio_inputs,
                     'outputs':f[1].__wideio_outputs,
                     'description':f[1].__wideio_description,
                     'authors':f[1].__wideio_authors,
                     })                                                  
                     
                     
def author_ref(parse_author):
    """
    Accepted format:
        - Ascii Ascii
        - Ascii Ascii <emailaddres@server>
        - Ascii Ascii [REFNUMBERWIOUSER@WIOSERVER] 
        - Ascii Ascii [!SHORTREF@WIOSERVER]         
    """
    pass
                     
#@_wio_command()
#def cmd_function_parameters(OPT,function):
#     print function



def wideio_main(function=None):
    """
    This functions transform any algorithm into a server.
    It allows program to be run as server, in request mode or from commandline for testing purpose.
    """
    #"--wideio_mode"
    #"--wideio_serialisation" e.g. enocder, decoder
    #"--wideio_deserialisation"
    #"--wideio_port" : default None if not mode=="server" else 26666

    # if None, we use defaults
    # default for --wideio-mode is 'request'
    # BSON in, JSON out
    OPTIONS=[
     ("wideio_mode","request", "defines how arguments are interpreted"),
     ("wideio_function",__wideio__function_register.items()[0][0], "name of the function to be ran"),
     ("wideio_port","26666", "the scheduler server port to contact."),
     ("wideio_serialisation","json", "defines how arguments are interpreted"),
     ("wideio_deserialisation","json", "defines how arguments are interpreted"),
     ("wideio_command","run", "must be one of "+"|".join(_COMMANDS.keys())),
     ("wide_help",__show_help_mesg , "shows this help message"),
     ("help",__show_help_mesg , "shows this help message"),
     ("define",__define_in_request , "override a request parameter"),
     ("wideio_debug","0", "defines the level of verbosity"),
    ]

    op=1
    aa=[]
    OPT=dict(map(lambda opt:opt[:2],OPTIONS))
    MODE=0
    while op<len(sys.argv):
        co=sys.argv[op]
        if MODE==0:
          if co.startswith("--") and OPT.has_key(co[2:]):
             #print co[2:],OPT.keys()
             if callable(OPT[co[2:]]):
                op+=OPT[co[2:]](sys.argv[op:],OPT,OPTIONS)
             else:
                OPT[co[2:]]=sys.argv[op+1]
                op+=2
          else:
            if co=="--":
                MODE=1
                op+=1
            else:
               sys.stderr.write("unsupported argument %s : %s --help for help\n"%(co, sys.argv[0]))
               exit(-1)
        else:
          op+=1
          properties = False
          if ':=' in co:
              co2=co.split(':=')
              co2[1] = "".join(co2[1:])
              co2[1] = co2[1].replace("'", '"')
              try:
                  co2[1] = json.loads(co2[1])
              except Exception as e:
                  print co2[1]
                  print '[error]:', e, 'in', co2[0]
                  exit(-1)
          elif '=' in co:
              co2=co.split("=")
              co2[1] = "".join(co2[1:])
          else:
              __override_params[co]=(True, 'bool')
              continue
          _type = None
          if '@' in co2[0]:
              _type = co2[0].split('@')
              co2[0] = _type[0]
              _type = _type[1]
          __override_params[co2[0]]=(co2[1], _type)
          # --------------- name --- value -- type  # < value can be dict or str


    for f in glob.glob("*.py"):
      # check if it is the current executed file
      if not f == sys.argv[0]:
        try:
          __import__(f[:-3], globals(), locals(), [], -1)
        except ImportError,e:
          sys.stderr.write("[warning] error while auto importing current python modules : %s\n"%(str(e),))


    if function==None:
      function=__wideio__function_register.items()[0][1]
    try:
      if (issubclass(function,object)):
        function=function() # Instantiate an instance
    except:
        pass

    if _COMMANDS.has_key(OPT["wideio_command"]):
        _COMMANDS[(OPT["wideio_command"])](OPT,function)
    else:
      sys.stderr.write( "[error] unsupported command")
      sys.exit(-1)
